import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Signup from '../views/Signup.vue'

import Home from '../components/admin/Home.vue'
import Patients from '../components/admin/Patients.vue'
import Appointment from '../components/admin/appointment/Appointment.vue'
import userAppointment from '../components/admin/appointment/SelectUserAppointment.vue'

import Dashboard from '../components/user/pages/Dashboard.vue'
import ClientAppointments from '../components/user/pages/Appointment.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: {
      hasUser: true
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Signup,
    meta: {
      hasUser: true
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    meta: {requiresLogin: true, isAdmin: true},
    component: () => import(/* webpackChunkName: "dashboard" */ '../components/admin/Dashboard.vue'),
    children: [
      {
        path: 'home',
        name: 'home',
        components: {
          home: Home
        }
      },
      {
        path: 'patients',
        name: 'patients',
        components: {
          patients: Patients
        }
      },
      {
        path: 'appointment',
        name: 'appointment',
        components: {
          appointment: Appointment
        }
      },
      {
        path: 'appointment/select',
        name: 'userAppointment',
        components: {
          userAppointment: userAppointment
        }
      },
    ]
  },
  {
    path: '/user',
    name: 'User',
    meta: {requiresLogin: true, isUser: true},
    component: () => import(/* webpackChunkName: "dashboard" */ '../components/user/pages/Home.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'userdashboard',
        components: {
          userdashboard: Dashboard
        }
      },
      {
        path: 'appointment',
        name: 'clientappointment',
        components: {
          clientappointment: ClientAppointments
        }
      },
      {
        path: '',
        to: 'dashboard'
      }
    ]
  }

]



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
	if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
    next({name: 'Login'})
  }
  else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
      next({ name: "Dashboard" });
	} 
  else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
      next({ name: "User" });
	} 
  else {
		next();
	}
});

export default router
