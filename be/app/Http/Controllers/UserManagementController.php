<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\BillingInfo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function summary(){
        $mytime = Carbon::now()->format('Y-m-d');

        $appointment = Appointment::whereDate('start', $mytime)->where('user_id', auth('api')->user()->id)->count();
        return response()->json(['appointment' => $appointment]);
    }

    public function index()
    {
        return response()->json(Appointment::with(['user', 'user.info'])->where('user_id', auth('api')->user()->id)->get());
    }

    public function storeAppointment(Request $request){
        $data = [
            'user_id' => auth('api')->user()->id,
            'type' => $request->type,
            'content' => $request->description,
            'start' => $request->time[0],
            'end' => $request->time[1],
            'status' => 'Pending'
        ];

        $hasAppointment = Appointment::where('start', '>=', $request->time[0])->where('end', '<=', $request->time[1])->where('user_id', auth('api')->user()->id)->first();
        if($hasAppointment){
            return response()->json(['msg' => 'Appointment at the specified schedule has an existing record!'], 422);
        }
        else {
            $appointment = Appointment::create($data);
            return response()->json(['msg' => 'Appointment added successfully!'], 200);
        }

    }

    public function cancelAppointment(Request $request, $id){

        $appointment = Appointment::where('id', $id)->first();

        if($appointment){
            if($appointment->status == 'Approved'){
                return response()->json(['msg' => 'Appointment cannot be cancelled'], 422);
            }
            else {
                $appointment->update(['status' => 'Cancelled']);
                return response()->json(['msg' => 'Appointment cancelled successfully!'], 200);
            }
        }

    }

}
